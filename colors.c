/*
  Copyright (C) 1999 - 2006 Florian Schintke
 
  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free 
  Software Foundation; either version 2, or (at your option) any later 
  version. 
 
  This is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
  for more details. 
 
  You should have received a copy of the GNU General Public License with 
  the c2html, java2html, pas2html or perl2html source package as the 
  file COPYING. If not, write to the Free Software Foundation, Inc., 
  59 Temple Place - Suite 330, Boston, MA 
  02111-1307, USA. 
*/

#include "colors.h"

char *bgcolor         = "\"#FFFFFF\"";
char *commentcolor    = "\"#B22222\"";
char *stringcolor     = "\"#666666\"";
char *definelinecolor = "\"#228B22\"";
char *preprolinecolor = "\"#A020F0\"";
char *keywordcolor    = "\"#4169E1\"";
char *labelcolor      = "\"#FF0000\"";
