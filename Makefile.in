# Copyright (C) 1999 - 2006 Florian Schintke
#
# This is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free 
# Software Foundation; either version 2, or (at your option) any later 
# version. 
#
# This is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
# for more details. 
#
# You should have received a copy of the GNU General Public License with 
# the c2html, java2html, pas2html or perl2html source package as the 
# file COPYING. If not, write to the Free Software Foundation, Inc., 
# 59 Temple Place - Suite 330, Boston, MA 
# 02111-1307, USA. 

SHELL = /bin/sh

.SUFFIXES:
# If COMPRESSION is not zero the executable will also work as
# a CGI-script. The number you specify will be passed to gzip
# as compression mode, if your browser can unzip it.
#
# It is recommended to use a not so high compression mode
# for gzip because otherwise the load on the web server
# grows without having big benefits.
# COMPRESSION = -DCOMPRESSION=4
COMPRESSION = -DCOMPRESSION=1

# Dont change things beyond this line.
TARGET          = @PROJECT_NAME@
VERSION         = @VERSION@
SRCS            = @srcdir@/mymain.c @srcdir@/colors.c
HDRS            = @srcdir@/colors.h @srcdir@/mymain.h 
LEXSRCS         = @srcdir@/$(TARGET).l
CONFIGS         = ./config.status Makefile config.h
MANPAGE	        = $(TARGET).1
LSM             = $(TARGET).lsm
DOCS            = AUTHORS COPYING NEWS README $(MANPAGE) $(LSM)
CONFIGIN        = @srcdir@/Makefile.in @srcdir@/configure.in @srcdir@/configure @srcdir@/install-sh @srcdir@/config.h.in
TMPCONFIGFILES  = config.cache config.status config.log config.h
FILENAME        = $(TARGET)-$(VERSION)

CC              = @CC@
LEX             = @LEX@
LEXLIB          = @LEXLIB@
LEXOUTPUT       = @LEX_OUTPUT_ROOT@.c
INSTALL         = @INSTALL@
INSTALL_PROGRAM = @INSTALL_PROGRAM@
INSTALL_DATA    = @INSTALL_DATA@
prefix          = @prefix@
exec_prefix     = ${prefix}
bindir          = @bindir@
mandir          = @mandir@
man1dir         = @mandir@/man1
srcdir          = @srcdir@
docdir          = @DOCDIR@
CPPFLAGS        = @CPPFLAGS@

CFLAGS += -O2 -Wall
ALL_CFLAGS = $(CFLAGS) -I$(srcdir) -I. $(COMPRESSION)

all: $(TARGET)

$(TARGET): $(LEXOUTPUT) $(SRCS) $(HDRS)
	$(CC) $(ALL_CFLAGS) -o $(TARGET) $(LEXOUTPUT) $(SRCS) $(LEXLIB)

$(LEXOUTPUT): $(LEXSRCS) $(HDRS)
	$(LEX) $(LEXSRCS)

install: $(TARGET) install-doc
	@echo "Installing" $(TARGET) "to" $(bindir)
	-mkdir -p $(bindir)
	$(INSTALL_PROGRAM) $(TARGET) $(bindir)/$(TARGET)

install-strip: $(TARGET) install-doc
	@echo "Installing" $(TARGET) "to" $(bindir)
	-mkdir -p $(bindir)
	$(INSTALL_PROGRAM) -s $(TARGET) $(bindir)/$(TARGET)

install-doc: 
	-mkdir -p $(man1dir) $(docdir)
	$(INSTALL_DATA) $(srcdir)/$(MANPAGE) $(man1dir)/$(MANPAGE)
	$(INSTALL_DATA) $(srcdir)/AUTHORS $(docdir)/AUTHORS
	$(INSTALL_DATA) $(srcdir)/COPYING $(docdir)/COPYING
	$(INSTALL_DATA) $(srcdir)/NEWS $(docdir)/NEWS
	$(INSTALL_DATA) $(srcdir)/README $(docdir)/README
	$(INSTALL_DATA) $(srcdir)/$(LSM) $(docdir)/$(LSM)

uninstall:
	rm -f $(bindir)/$(TARGET)
	rm -f $(man1dir)/$(MANPAGE)
	rm -rf $(docdir)

clean:
	rm -rf *.o *~ $(LEXOUTPUT) TAGS

distclean:
	rm -rf *.o *~ Makefile $(LEXOUTPUT) $(TARGET) $(TMPCONFIGFILES) TAGS

# stuff to update Makefile when changing configuration

$(srcdir)/configure: $(srcdir)/configure.in
	cd $(srcdir) && autoconf

Makefile: $(srcdir)/Makefile.in $(srcdir)/config.h.in
	@echo "regeneration with (in ./config.status) saved configure results..."
	./config.status
	@echo
	@echo Please rerun make so it will use the updated Makefile.
	exit 1

./config.status: $(srcdir)/configure
	$(srcdir)/configure
	@echo
	@echo Please rerun make so it will use the updated Makefile.
	exit 1

# stuff to create a distribution
# also updates the .lsm file with version and size of archive
dist: $(CONFIGIN) $(LEXSRCS) $(SRCS) $(HDRS) $(DOCS)
#	@echo This only works if you have not changed
#	@echo the version number directly before.
#	@echo If so, start a make without parameters first.
	@echo
	@echo Will create
	@echo $(FILENAME).tar.gz
	@echo
	@echo 'Type CTRL-C to abort (you have 5 seconds)!'
	@sleep 5  
	@echo "updating" $(TARGET).lsm
	@rm -rf $(FILENAME)
	@mkdir $(FILENAME)
	@cp $(LEXSRCS) $(SRCS) $(HDRS) $(DOCS) $(CONFIGIN) $(FILENAME)
	@tar -czf $(FILENAME).tar.gz $(FILENAME)
	@sed -e "s/^Version:.*/Version:        $(VERSION)/" \
	    -e "s/^Entered-date:.*/Entered-date:   `date \
			'+%d%b%y' | tr '[a-z]' '[A-Z]'`/" \
	    -e "s/$(TARGET)-.*.tar.gz/$(FILENAME).tar.gz/" \
	    -e "s/[0-9]*kB $(FILENAME).tar.gz/`du \
		 -k $(FILENAME).tar.gz\
		  | cut -f1`kB $(FILENAME).tar.gz/" \
	    < $(TARGET).lsm > $(FILENAME)/$(TARGET).lsm
	@cp $(FILENAME)/$(TARGET).lsm .
	@echo "creating" $(FILENAME).tar.gz
	@tar -czf $(FILENAME).tar.gz $(FILENAME)
