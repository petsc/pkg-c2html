/*
  Copyright (C) 1999 - 2006 Florian Schintke
  Copyright (C) 1999       Martin Kammerhofer for the CGI feature
  Copyright (C) 2000       Rob Ewan           for the indexing feature
 
  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free 
  Software Foundation; either version 2, or (at your option) any later 
  version. 
 
  This is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
  for more details. 
 
  You should have received a copy of the GNU General Public License with 
  the c2html, java2html, pas2html or perl2html source package as the 
  file COPYING. If not, write to the Free Software Foundation, Inc., 
  59 Temple Place - Suite 330, Boston, MA 
  02111-1307, USA. 
*/

/* This .h file contains some functions implemented in 
   .l file from the lex scanner or in the mymain.c */

/*
  Functions implemented in the .l file and called
  from mymain.c
*/
extern void StartNewYylex(FILE * in, FILE * out);

/* 
  Functions implemented in the mymain.c file and called
  from the .l file
*/
enum weight_enum {NO_CHANGE, NORMAL, BOLD}; 
typedef enum weight_enum weight_type;

typedef struct {
  /* This variables are initialized in MyMain() in mymain.c */
  int   nocgi;       /* != 0 must not generate HTTP headers; set with -c */
  int   noheaders;   /* != 0 suppress html headers;          set with -s */
  int   linelabeling;/* != 0 label lines with numbers;       set with -n */
  int   skipblanks;  /* != 0 skip blank line labeling;       set with -N */
  int   width;       /* width of output (default 80);        set with -w */
  int   indexOnly;   /* != 0, only output generated NAME's   set with -i */
  char  tab[81];     /* tab expansion (default 8 spaces);    set with -x */
  char  *title;      /* title of the generated html file;    set with -t */
  char  *prog;       /* name of the program running                      */
  char  *headfile;   /* file inserted before converting;     set with -h */
  char  *bottomfile; /* file inserted after converting;      set with -b */
  /* internally used while converting files */
  /* the following part must be reinitialized if we start a new file */
  /* this is done in the function StartNewYylex() */
  int         lineNumber;
  int         needLabel;
  char        *currentColor;
  weight_type currentWeight;
  int         suppressOutput; /* Variable - used to control output for -i */
} config_type;

extern config_type config;
int
MyMain(int argc, char *argv[]);
void
MyStringOutput(FILE *, char *);

void 
EndLabelTag( FILE *, char *); /* For indexing */

/* Add a label a function */
/* Therefor search for the first opening parenthesis */
/* and use the word before as label name. */
void
AddLabelForFunction(FILE *, char *);

/* Add a label for a class */
void
AddLabelForClass(FILE *, char *);

/* Add a label for a struct */
void
AddLabelForStruct(FILE *, char *);

/* Change the font and the weight
 * If color is a NULL pointer the FONT will be closed if
 * one is opened. 
 * Generate the tags alway so that FONT is contained in an
 * STRONG if weight is BOLD.
 * Generate the tags in a flat structure (no nested fonts)
 */
void
ChangeFontTo(FILE *out, char *color, weight_type weight);

/* 
  Functions implemented in the mymain.c file and called
  from the mymain.c file
*/

/* 
 * insert the file "filename" in the current directory (or if given the 
 * complete path this file) into the output "outfile".
 */
int 
Insert (FILE * outfile, char *filename);

/*
 * print usage information
 */
void 
PrintUsage();

/* 
 * Parse parameters and set the configuration in global
 * variable config. Return the number of commandline 
 * parameters parsed, so the caller can skip them.
 */
int
ParseParameters(int argc,char *argv[]);

/*
 * print the current configuration
 */
void 
PrintConfig(FILE * output);

/*
 * decide whether CGI script or not
 */
int
IsCGI();

/*
 * write CGI header
 */
void
PrintCGIHeader();

/*
 * write html headers if necessary
 * insert head and bottom file if recommended
 * convert the file from actin and give it to actout
 * use name as title if title is not set explicit
 * use "stdin" as title if name is NULL
 */
void 
ConvertFile(char * name);
