/*
  Copyright (C) 1999 - 2006 Florian Schintke
  Copyright (C) 1999       Martin Kammerhofer for the CGI feature
  Copyright (C) 2000       Rob Ewan           for the indexing feature

  This is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free 
  Software Foundation; either version 2, or (at your option) any later 
  version. 

  This is distributed in the hope that it will be useful, but WITHOUT 
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License 
  for more details. 

  You should have received a copy of the GNU General Public License with 
  the c2html, java2html, pas2html or perl2html source package as the 
  file COPYING. If not, write to the Free Software Foundation, Inc., 
  59 Temple Place - Suite 330, Boston, MA 
  02111-1307, USA. 
*/

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <locale.h>

/* getopt is contained in unistd If we have no unistd, 
   we can try to include getopt */
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#elif HAVE_GETOPT_H
#include <getopt.h>
#endif

#include "mymain.h"
#include "colors.h"

#ifndef COMPRESSION
#define COMPRESSION 0
#endif

/* exit values */
#define CANNOT_READ_INPUT   1
#define CANNOT_WRITE_OUTPUT 2
#define PRINTED_USAGE       3
#define GZIP_NOT_FOUND      4

/* global variables */
FILE *actin;
FILE *actout;
config_type config;

int
MyMain(int argc, char *argv[])
{
  char *outfilename      = (char *) NULL;
  char *temp_ch_ptr      = (char *) NULL;
  int  i                 = 0;
  int  parsed_parameters = 0;
  int  return_code       = 0;

  (void) setlocale(LC_TIME, "C");
  /* initialize configuration with defaults */
  config.nocgi        = 0;
  config.noheaders    = 0;
  config.linelabeling = 0;
  config.skipblanks   = 1;
  config.width        = 80;
  config.indexOnly    = 0;
  config.title        = (char *) NULL;
  temp_ch_ptr         = strrchr(argv[0], '/');
  config.prog         = (temp_ch_ptr)? ++temp_ch_ptr : argv[0]; /* basename */
  temp_ch_ptr         = (char *) NULL;
  config.headfile     = (char *) NULL; 
  config.bottomfile   = (char *) NULL; 
  strcpy(config.tab, "        ");

  /* initialize configuration with given parameters */
  parsed_parameters = ParseParameters(argc, argv);
  argc -= parsed_parameters;
  argv += parsed_parameters;

  /* do the job */
  if (0 == argc) 
    {
      actin  = stdin;
      actout = stdout;
      if (IsCGI())
	{ PrintCGIHeader(); }
      ConvertFile("");
    } 
  else 
    {
      for (i = 0; i < argc; i++) 
	{
	  actin = fopen (argv[i], "r");
	  if (!actin) 
	    {
	      fprintf (stderr, "%s: cannot read file '%s'\n", 
		       config.prog, argv[i]);
	      return_code = return_code < CANNOT_READ_INPUT ? 
		CANNOT_READ_INPUT : return_code;
	      continue;
	    }
	  outfilename = malloc (sizeof (char)*strlen (argv[i]) + 6);
	  if (NULL == outfilename)
	    { fprintf (stderr, "Cannot allocate enough memory.\n"); }
	  strcat (strcpy (outfilename, argv[i]), ".html");
	  actout = fopen (outfilename, "w");
	  if (!actout) 
	    {
	      fprintf (stderr, "%s: cannot write file '%s'\n", 
		       config.prog, outfilename);
	      return_code = return_code < CANNOT_WRITE_OUTPUT ? 
		CANNOT_WRITE_OUTPUT : return_code;
	      continue;
	    }
	  ConvertFile(argv[i]);
	  
	  free (outfilename);
	  outfilename = (char *) NULL;
	  fclose (actin);
	  fclose (actout);
	}
    }
  return return_code;
}

int
Insert (FILE * outfile, char *filename)
{
  int c;
  FILE * infile;
 
  infile = fopen (filename, "r");
  if (NULL != infile)
    {
      c = fgetc (infile);
      while (EOF != c)
        { 
          fputc (c, outfile);
          c = fgetc (infile);
        }
      fclose (infile);
      return 1;
    }
  else
    { return 0; }
}

void 
PrintUsage() 
{
  fprintf (stderr,
           "usage: %s [options] [file_to_convert ...]\n", 
           config.prog);
  fprintf (stderr, "  -c       switch off CGI detection and HTTP header generation\n");
  fprintf (stderr, "  -s       suppress generation of HTML headers\n");
  fprintf (stderr, "  -n       number lines and label them, skipping blank lines\n");
  fprintf (stderr, "  -N       number all lines and label them\n");
  fprintf (stderr, "  -i       generate an HTML index of set labels\n");
  fprintf (stderr, "             (only in c2html and java2html)\n");
  fprintf (stderr, "  -V       print version information\n");
  fprintf (stderr, "  -u       print this usage information\n");
  fprintf (stderr, "  -t title set HTML title\n");
  fprintf (stderr, "  -w width use width for output (default 80), 0 to disable\n");
  fprintf (stderr, "  -x size  use size for tab size (default 8, max 80)\n");
  fprintf (stderr, "  -h file  insert file at the head of converted data\n");
  fprintf (stderr, "  -b file  insert file at the bottom of converted data\n");
  fprintf (stderr, "  --       interpret all following parameters as filenames\n");

  PrintConfig(stderr);
  exit(PRINTED_USAGE);
}

void 
PrintConfig(FILE * output)
{
  fprintf (output, "Current config is:\n");
  fprintf (output, "  (end of strings are marked with \"\\0\" and newline)\n");
  fprintf (output, "  COMPRESSION : %d\n", 
	   COMPRESSION);  
  fprintf (output, "  nocgi       : %s\n", 
	   config.nocgi ? "on" : "off");  
  fprintf (output, "  noheaders   : %s\n", 
	   config.noheaders ? "on" : "off");  
  fprintf (output, "  linelabeling: %s\n", 
	   config.linelabeling ? "on" : "off");  
  fprintf (output, "  skipblanks  : %s\n",
	   config.skipblanks ? "on" : "off");
  fprintf (output, "  width       : %d\n", 
	   config.width);  
  fprintf (output, "  tabsize     : %zu\n",
	   strlen(config.tab));
  fprintf (output, "  title       : %s%s\n", 
	   config.title ? config.title : "(null)", 
	   config.title ? "\\0" : "");  
  fprintf (output, "  prog        : %s%s\n", 
	   config.prog ? config.prog : "(null)",
	   config.prog ? "\\0" : "");  
  fprintf (output, "  headfile    : %s%s\n", 
	   config.headfile ? config.headfile : "(null)",  
	   config.headfile ? "\\0" : "");  
  fprintf (output, "  bottomfile  : %s%s\n", 
	   config.bottomfile ? config.bottomfile : "(null)",
	   config.bottomfile ? "\\0" : "");  
  fprintf (output, "  index       : %s\n",
	   config.indexOnly ? "on" : "off" );
}

int
ParseParameters(int argc,char *argv[])
{
  int ch = 0;
  int size;

  while (-1 != (ch = getopt(argc, argv, "t:w:x:h:b:nNicusV")))
    {
      switch(ch) 
	{
	case 't':
	  config.title = optarg;
	  break;
	case 'h':
	  config.headfile = optarg;
	  break;
	case 'b':
	  config.bottomfile = optarg;
	  break;
	case 'w':
	  if (1 != sscanf(optarg, "%d", &config.width))
	    { PrintUsage(); }
	  break;
	case 'x':
	  if (1 != sscanf(optarg, "%d", &size))
	    { PrintUsage(); }
	  if (size > 80)
	    { PrintUsage(); }
	  config.tab[size--] = 0x0;
	  for (; size>0; size--)
	    { config.tab[size] = ' '; }
	  break;
	case 'n':
	  /* Switch on line numbering and labeling */
	  config.linelabeling = 1;
	  config.skipblanks = 1;
	  break;
	case 'N':
	  /* Switch on line numbering and labeling of all lines */
	  config.linelabeling = 1;
	  config.skipblanks = 0;
	  break; 
	case 'i':
	  /* Turn on index generation (and initially suppress output) */
	  config.indexOnly = 1;
	  config.suppressOutput = 1;
	  break;
        case 'c':
	  /* suppress generation of CGI header */
          config.nocgi = 1;
          break;
        case 's':
	  /* suppress generation of html headers */
          config.noheaders = 1;
          break;
	case 'u':
	  PrintUsage();
	  break;
	case 'V':
	  fprintf(stdout, "%s %s%s\n",
		  PROJECT_NAME,
		  VERSION, 
                  COMPRESSION ? "" : " (no compression)");
	  exit(0);
	default:
	  PrintUsage();
	}
    }

  return optind;
}

int
IsCGI()
{
  return (getenv("PATH_TRANSLATED")
    && getenv("GATEWAY_INTERFACE") 
    && 0 == config.nocgi);
}

void
PrintCGIHeader()
{
  char       *pt = (char *) NULL;
  struct stat sb;
  char        gzipcmd[10];

  /* CGI */
  pt = getenv("PATH_TRANSLATED");
  if (!config.title)
    { config.title = pt; }
  actin = fopen (pt, "r");
  if (!actin) 
    {
      fprintf (actout, "Content-Type: text/html\n\n");
      fprintf (actout, "<html><head><title>error in CGI '%s'", config.prog);
      fprintf (actout, "</title></head>\n");
      fprintf (actout, "<body><h1>error in CGI program '%s': ", config.prog);
      fprintf (actout, "cannot read file '%s'</h1></body>\n", pt);
      fprintf (actout, "</html>\n");
      exit(1);
    }
  fprintf (actout, "Content-Type: text/html\n");
  if (fstat(fileno(actin), &sb) == 0) {
    /* report last modification date */
    char rfc1123date[32];
    
    if (strftime(rfc1123date, sizeof(rfc1123date),
		 "%a, %e %b %Y %T GMT", gmtime(&sb.st_mtime)))
      fprintf(actout, "Last-Modified: %.*s\n",
	      (int)sizeof(rfc1123date), rfc1123date);
  }
  if (COMPRESSION)
    {
      /* should we compress output with gzip? */
      do 
	{
	  if ((pt = getenv("REMOTE_ADDR")) 
	      && (!strncmp(pt, "127", 3)))
	    { break; } /* never compress to local client */
	  if (!(pt = getenv("HTTP_ACCEPT_ENCODING")))
	    { break; } /* no header Accept-Encoding: */
	  if (!strstr(pt, "gzip"))
	    { break; } /* browser doesn't understand gzip format */
	  if (strstr(pt, "x-gzip"))
	    { fprintf(actout, "Content-Encoding: x-gzip\n\n"); }
	  else /* some WWW browsers need this (namely ie) */
	    { fprintf(actout, "Content-Encoding: gzip\n\n"); }
	  fflush(actout);
	  sprintf(gzipcmd, "gzip -%d", COMPRESSION);
	  if (!(actout = (FILE *) popen(gzipcmd, "w")))
	    { exit(GZIP_NOT_FOUND); } /* cannot fork, gzip not found, ... */
	} 
      while (0);
    }
}

void 
ConvertFile(char * name)
{
  struct stat sb;

  if (0 == config.noheaders)
    {
      char * title = (char *) NULL;
      
      if (NULL == name)
	{ title = config.title ? config.title : "stdin"; }
      else
	{ title = config.title ? config.title : name; }

      fprintf (actout, "\n<html>\n<head>\n");
      fprintf (actout, "<title>%s</title>\n", title);
      fprintf (actout, "<meta name=\"generator\" content=\"%s %s%s\">\n",
	       PROJECT_NAME,
	       VERSION,
               COMPRESSION ? "" : " (no compression)");
      if (fstat(fileno(actin), &sb) == 0) {
	  /* report last modification date */
	  char iso8601date[32];
    
	  if (strftime(iso8601date, sizeof(iso8601date),
		       "%Y-%m-%dT%H:%M:%S+00:00", gmtime(&sb.st_mtime)))
	      fprintf(actout, "<meta name=\"date\" content=\"%.*s\">\n",
		      (int)sizeof(iso8601date), iso8601date);
      }
      fprintf (actout, "</head>\n\n");
      fprintf (actout, "<body bgcolor=%s>\n", bgcolor);
    }

  Insert (actout, config.headfile);
  if (0 < config.width)
    { fprintf (actout, "<pre width=\"%d\">", config.width); }
  else
    { fprintf (actout, "<pre>"); }
  StartNewYylex(actin, actout);
  
  fprintf (actout, "</pre>\n");
  Insert (actout, config.bottomfile);
  if (0 == config.noheaders)
    { fprintf (actout, "</body>\n\n</html>\n"); }
}

void MyStringOutput(FILE * out, char * myString)
{
  static char        *previousColor = NULL;
  static weight_type previousWeight = NORMAL;
  char   ch = 0;
  
  if (1 == config.suppressOutput) 
    { return; }
  if (NULL == myString)
    { return; }

  while('\0' != myString[0])
    {
      ch = *myString;
      myString++;
      if ('\r' == ch)
        continue;
      if (0 == config.linelabeling)
	{
	  if ('\t' == ch)
	    { fprintf(out, config.tab); }
	  else
	    { fputc(ch, out); }
	}
      else
	{
	  /* Create labeled lines */	  
	  if ('\n' != ch)
	    { 
	      if (1 == config.needLabel)
		{ 
		  config.needLabel = 0;
		  fprintf(out, "<a name=\"line%d\">%3d: </a>", 
			  config.lineNumber, config.lineNumber);
		  ChangeFontTo(out, previousColor, previousWeight);
		}
	      if ('\t' == ch)
		{ fprintf(out, config.tab); }
	      else
		{ fputc(ch, out); }
	    }
	  else
	    { 
	      if (0 == config.needLabel)
		{
		  /* Do this only once for succeeding */
		  /* newlines */
		  previousColor  = config.currentColor;
		  previousWeight = config.currentWeight;
		}
	      if (0 == config.skipblanks && 1 == config.needLabel)
		{ 
		  config.needLabel = 0;
		  fprintf(out, "<a name=\"line%d\">%3d: </a>", 
			  config.lineNumber, config.lineNumber);
		  ChangeFontTo(out, previousColor, previousWeight);
		}

	      ChangeFontTo(out, NULL, NORMAL);
	      fputc(ch, out);
	      config.needLabel = 1;
	      config.lineNumber++;
	    }
	}
    }
}

/* Change the font and the weight
 * If color is a NULL pointer the FONT will be closed if
 * one is opened. 
 * Generate the tags so that FONT is contained in an
 * STRONG if weight is BOLD.
 * Generate the tags in a flat structure (no nested fonts)
 */
void
ChangeFontTo(FILE *out, char *color, weight_type weight)
{
  switch(weight)
    {
    case NO_CHANGE:
      break;
    case BOLD:
      if (BOLD != config.currentWeight)
	{ 
	  if (NULL != config.currentColor)
	    {
	      MyStringOutput(out, "</font>");
	      config.currentColor = NULL;
	    }
	  MyStringOutput(out, "<strong>"); 
	  config.currentWeight = BOLD;
	}
      break;
    case NORMAL:
      if (NORMAL != config.currentWeight)
	{ 
	  if (NULL != config.currentColor)
	    {
	      MyStringOutput(out, "</font>");
	      config.currentColor = NULL;
	    }	  
	  MyStringOutput(out, "</strong>"); 
	  config.currentWeight = NORMAL;
	}
      break;
    default:
      fprintf (stderr, "internal error: bad weight\n");
    }

  if (color != config.currentColor)
    {
      if (NULL != config.currentColor)
	{
	  MyStringOutput(out, "</font>");
	  config.currentColor = NULL;
	}
      if (NULL != color)
	{
	  MyStringOutput(out, "<font color=");
	  MyStringOutput(out, color);
	  MyStringOutput(out, ">");
	  config.currentColor = color;
	}
    }
  /* Check for consistency */
  if (color != config.currentColor)
    { fprintf(stderr, "internal error in ChangeFontTo\n"); }
}

void
AddLabelTag(FILE *out, char *start_ptr, char *end_ptr)
{
  if (start_ptr == end_ptr)
    { return; }
  
  /* If we're only generating an index, */
  if (config.indexOnly) 
    {
      /* Output it as a list item and as a reference */
      fprintf(out, "<li><a href=\"#");
      /* Turn on output for the function name */
      config.suppressOutput = 0;
    } 
  else 
    {
      /* Output it as a target */
      fprintf(out, "<a name=\"");
    }
  while (start_ptr != end_ptr)
    { 
      fputc(*start_ptr, out);
      start_ptr++;
    }
 
  /* If we're generating an index, */
  if (config.indexOnly) 
    {
      /* Don't close the HREF yet */
      fprintf(out, "\">");
    } 
  else 
    {
      /* Close the NAME */
      fprintf(out, "\"></a>");
    }
}

void 
EndLabelTag(FILE *out, char *text)
{
  /* If we're just generating an index, */
  if (config.indexOnly)
    {
      /* End the HREF */
      fprintf(out, "</a></li>\n");
      /* And suppress further output */
      config.suppressOutput = 1;
    }
}

/* Add a label a function */
/* Therefore search for the first opening parenthesis */
/* and use the word before as label name. */
void
AddLabelForFunction(FILE *out, char *text)
{
  char *start_ptr = NULL;
  char *end_ptr   = NULL;

  end_ptr = strchr(text, '(');
  if (NULL == end_ptr)
    { return; }

  end_ptr--;
  while ((' ' == *end_ptr)
	 || ('\t' == *end_ptr))
    { end_ptr--; }
  start_ptr = end_ptr;
  while (((('A' <= *start_ptr) && ('Z' >= *start_ptr))
	  || (('a' <= *start_ptr) && ('z' >= *start_ptr))
	  || (('0' <= *start_ptr) && ('9' >= *start_ptr))
	  || ('_' == *start_ptr))
	 && (start_ptr != text))
    { start_ptr--; }
  start_ptr++;
  end_ptr++;
  AddLabelTag(out, start_ptr, end_ptr);
}

/* Add a label for a class */
/* Therefore search for the word class and */
/* use the word after that as label name. */
void
AddLabelForClass(FILE *out, char *text)
{
  char *start_ptr = NULL;
  char *end_ptr   = NULL;

  start_ptr = strstr(text, "class");
  if (NULL == start_ptr)
    { return; }

  /* move the pointer behind the word 'class' */
  start_ptr += 5;
  while ((' ' == *start_ptr)
	 || ('\t' == *start_ptr))
    { start_ptr++; }
  end_ptr = start_ptr;
  while (((('A' <= *end_ptr) && ('Z' >= *end_ptr))
	      || (('a' <= *end_ptr) && ('z' >= *end_ptr))
	  || (('0' <= *end_ptr) && ('9' >= *end_ptr))
	  || ('_' == *end_ptr)))
    { end_ptr++; }
  AddLabelTag(out, start_ptr, end_ptr);
}

/* Add a label for a struct */
/* Therefore search for the word 'struct' and */
/* use the word after that as label name. */
/* 99-12-15: Cloned from AddLabelForClass -- RE */
void
AddLabelForStruct(FILE *out, char *text)
{
  char *start_ptr = NULL;
  char *end_ptr = NULL;

  start_ptr = strstr(text, "struct");
  if (NULL == start_ptr)
    { return; }
  
  /* move the pointer behind the word 'struct' */
  start_ptr += 6;
  while ((' ' == *start_ptr)
	 || ('\t' == *start_ptr))
    { start_ptr++; }
  end_ptr = start_ptr;
  while (((('A' <= *end_ptr) && ('Z' >= *end_ptr))
	  || (('a' <= *end_ptr) && ('z' >= *end_ptr))
	  || (('0' <= *end_ptr) && ('9' >= *end_ptr))
	  || ('_' == *end_ptr)))
    { end_ptr++; }
  AddLabelTag(out, start_ptr, end_ptr);
}
